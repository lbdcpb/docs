# Configuración

## Índice

- [1. Objetivo](#1-objetivo)
- [2. Situación inicial](#2-situación-inicial)
- [3. Proceso](#3-proceso)
  - [3.1. Organización](#31-organización)
  - [3.2. Caves SSH](#32-caves-ssh)
  - [3.3. Claves GPG](#33-claves-gpg)
  - [3.4. Ficheros <code>.gitconfig</code>](#34-ficheros-gitconfig)
  - [3.5. Próximos pasos](#35-próximos-pasos)
- [4. Conclusiones](#4-conclusiones)
- [5. Enlaces](#5-enlaces)

## 1. Objetivo

Establecer una configuración para el trabajo con diferentes cuentas de proveedores de alojamiento de código (GitLab, GitHub, entre otros).

## 2. Situación inicial

Como desarroladores, en algún momento, hemos accedido a GitHub (público o empresarial) o GitLab (público o privado autogestionado) con credenciales creadas por nosotros mismos o entregadas como parte de la colaboración con algún cliente.

Aunque parezca un poco extremo, voy a contar la situación en la que me encontraba hace un tiempo atrás:

- Colaboraba con un cliente que tiene sus repositorios en GitLab autogestionado y me proporcionó una dirección de correo y contraseña.

- Mi portfolio estaba en una cuenta de GitHub creada con mi cuenta de correo personal.

- Unos amigos me pidieron ayuda para unas pruebas de concepto y tenían sus repositorios en GitLab, así que tuve que crear una cuenta con mi correo personal.

- La universidad solicitó que para la sustentación de un proyecto entregue acceso a un repositorio público para la revisión del código, así que creé otra cuenta en GitLab con el correo de la universidad.

## 3. Proceso

Una vez entendida la situación seguí los siguientes pasos:

### 3.1. Organización

Cada cuenta contaría con un directorio donde manejar los diferentes proyectos asociados a cada una de ellas:

```bash
> ls 
.
├── ...
├── Work
├── Personal
├── Labs
├── Universidad
├── ...
```

### 3.2. Caves SSH

Desde siempre he trabajado con claves ssh para realizar las operaciones contra el repositorio remoto, cada plataforma te brinda ayuda y un lugar donde dejarlas configuradas:

- https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html

- https://docs.github.com/en/authentication/connecting-to-github-with-ssh

```bash
# Comando base
> ssh-keygen -t ed25519  -f ~/<Folder>/<platform>.<folder_name>-ssh -C "<platform> <mailaccount>"

# Work
> ssh-keygen -t ed25519  -f ~/Work/gitlab.work-ssh -C "gitlab usuario@work.com"

# Personal
> ssh-keygen -t ed25519  -f ~/Personal/github.personal-ssh -C "github mypersonalemail@email.com"

# Labs
> ssh-keygen -t ed25519  -f ~/Labs/gitlab.labs-ssh -C "gitlab mypersonalemail@email.com"

# Universidad
> ssh-keygen -t ed25519  -f ~/Universidad/gitlab.universidad-ssh -C "gitlab myStudentEmail@university.edu.pe"

```

El resultado:

```bash
├── ...
├── Work
│   ├── gitlab.work-ssh
│   ├── gitlab.work-ssh.pub
│   ├── ...
├── Personal
│   ├── github.personal-ssh
│   ├── github.personal-ssh.pub
│   ├── ...
├── Labs
│   ├── gitlab.labs-ssh
│   ├── gitlab.labs-ssh.pub
│   ├── ...
├── Universidad
│   ├── gitlab.universidad-ssh
│   ├── gitlab.universidad-ssh.pub
│   ├── ...
├── ...
```

Luego toca ir a registrar las claves en cada plataforma:

- https://github.com/settings/keys

    ![GitHub-ssh-key](../imgs/github-sshkey-gpgkey.png "GitHub ssh-key page")


- https://gitlab.com/-/profile/keys

    ![GitLab-ssh-key](../imgs/gitlab-sshkey.png "GitLab ssh-key page")


Tras lo anterior, es necesario editar el fichero <code>~/.ssh/config</code> para establecer qué claves serán tomadas según el sitio:

```bash
# Work Account Identity
Host gitlab.work.com
  Hostname gitlab.work.com
  User git
  IdentityFile ~/Work/gitlab.work-ssh

# Personal Account Identity
Host github.com
  Hostname github.com
  User git
  IdentityFile ~/Personal/github.personal-ssh

# Labs Account Identity
Host labs.gitlab.com
  Hostname gitlab.com
  User git
  IdentityFile ~/Labs/gitlab.labs-ssh

# University Account Identity
Host universidad.gitlab.com
  Hostname gitlab.com
  User git
  IdentityFile ~/Universidad/gitlab.universidad-ssh

Host *
  AddKeysToAgent yes
  IdentitiesOnly yes
  PreferredAuthentications publickey
  Compression yes
```

Donde:

- Con la opción <code>IdentityFile</code> se establece la ubicación de la clave privada.

- Cuando el <code>Hostname</code> es el mismo para dos cuentas (como en Labs y Universidad) se crean <code>Host</code> con nombres para diferenciarlos.

### 3.3. Claves GPG

Dependiendo de la forma de trabajar de cada lugar se nos pedirá configurar claves GPG para firmar los commits, aquí también cada plataforma te brinda ayuda y un lugar donde dejarlas configuradas:

- https://docs.github.com/en/authentication/managing-commit-signature-verification

- https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html

En mi caso fue necesario crear las claves para Work y Universidad con el respectivo correo y luego registrar el ID:

```bash
# Generación de clave GPG, se debe usar el EMAIL verificado por la plataforma
> gpg --full-gen-key

# Obtención de ID a utilizar en fichero .gitconfig
> gpg --list-secret-keys --keyid-format LONG <EMAIL>

# Obtención de clave pública para registrar en la plataforma
> gpg --armor --export <ID>
```

Al igual que para las claves SSH, es necesario ir a registrar las claves GPG en los sitios correspondientes:


- https://github.com/settings/keys

    ![GitHub-gpg-key](../imgs/github-sshkey-gpgkey.png "GitHub gpg-key page")

- https://gitlab.com/-/profile/gpg_keys

    ![GitLab-gpg-key](../imgs/gitlab-gpgkey.png "GitLab gpg-key page")

### 3.4. Ficheros <code>.gitconfig</code>

Toca crear una configuración de git para cada plataforma, hacemos esto para poder personalizar ciertos valores una sola vez y sin entrar en conflicto con valores globales:

- Fichero <code>~/.gitconfig.work</code>

    ```bash
    [commit]
        gpsign = true
        gpgsign = true

    [user]
        name = Lastname Name
        email = usuario@work.com
        signingkey = GPG_ID_FOR_THIS_EMAIL
    ```

- Fichero <code>~/.gitconfig.personal</code>

    ```bash
    [user]
        name = Name
        email = mypersonalemail@email.com
    ```

- Fichero <code>~/.gitconfig.labs</code>

    ```bash
    [user]
        name = Nickname
        email = mypersonalemail@email.com

    [url "git@labs.gitlab.com"]
    insteadOf = git@gitlab.com
    ```

- Fichero <code>~/.gitconfig.universidad</code>

    ```bash
    [commit]
        gpsign = true
        gpgsign = true
    [user]
        name = LastName, Name
        email = myStudentEmail@university.edu.pe
        signingkey = GPG_ID_FOR_THIS_EMAIL

    [url "git@universidad.gitlab.com"]
    insteadOf = git@gitlab.com
    ```

- Fichero <code>~/.gitconfig</code>

    ```bash
    [core]
            excludesfile = ~/.gitignore_global

    [init]
            defaultBranch = main

    [user]
            useConfigOnly = true

    [includeIf "gitdir:~/Work/"]
            path = ~/.gitconfig.work

    [includeIf "gitdir:~/Personal/"]
            path = ~/.gitconfig.personal

    [includeIf "gitdir:~/Labs/"]
            path = ~/.gitconfig.labs

    [includeIf "gitdir:~/Universidad/"]
            path = ~/.gitconfig.universidad
    ```

### 3.5. Próximos pasos

A continuación dejo una lista de comandos para verificación y validación de los repositorios, así como resolución de problemas conocidos:

- Validar la configuración aplicada en los proyectos:

    ```bash
    # Muestra las configuraciones y su procedencia
    > git config --show-origin --list
    ```

- Verificación de la URL del remoto

    ```bash
    # Muestra las direcciones completas
    > git remote -v
    origin	git@labs.gitlab.com:group/subgroup/project.git (fetch)
    origin	git@labs.gitlab.com:group/subgroup/project.git (push)
    ```

- Si no aparece el commit firmado hay que forzarlo añadiendo -S en el comando:

    ```bash
    git commit -S -m "YOUR_COMMIT_MESSAGE"
    ```

- Problemas con el firmado de commits: <code>git commit</code> devuelve error <code>gpg failed to sign data</code> en macOS:

    ```bash
    # Instalación
    > brew install pinentry-mac

    # Configuración en GPGP
    > echo "pinentry-program $(which pinentry-mac)" >> ~/.gnupg/gpg-agent.conf

    # Limpiar agentes gpg
    > killall gpg-agent
    ```

- Problemas con el certificado: devuelve el error <code>SSL certificate problem : self signed certificate in certification chain</code> tras intentar clonar un repositorio:

    ```bash
    # Pasos a seguir: 
    #  - Abrir Firefox y navegar a la opción de ver certificados.
    #  - Pulsar "Añadir excepción..." desde la pestaña "Servidores".
    #  - Pegar la url del repositorio y pulsar "Obtener certificado".
    #  - Se abrirá una ventana emergente con el detalle del certificado, ir hasta la parte "Misceláneo" y descargar en una ruta conocida.
    #  - Una vez obtenidos los certificados tenemos la opción de establecerlo de forma global o en el fichero .gitconfig de nuestra preferencia:

    # Comando global en Windows
    > git config --global http.sslCAInfo "C:\path\to\certs\cert-chain.pem"

    # Comando global en MacOS & Linux
    > git config --global http.sslCAInfo /path/to/certs/cert-chain.pem

    # En fichero .gitconfig en Windows
    [http]
	    sslCAInfo = C:\\path\\to\\certs\\cert-chain.pem

    # En fichero .gitconfig en MacOS & Linux
    [http]
        sslCAInfo = /path/to/certs/cert-chain.pem
    ```

## 4. Conclusiones

Finalmente la estructura de directorios quedó de la siguiente manera:

```bash
.
├── ...
├── .gitconfig
├── .gitconfig.work
├── .gitconfig.personal
├── .gitconfig.labs
├── .gitconfig.universidad
├── .gitignore_global
├── .gnupg
│   ├── gpg-agent.conf
│   ├── ...
├── .ssh
│   ├── config
│   ├── ...
├── Work
│   ├── gitlab.work-ssh
│   ├── gitlab.work-ssh.pub
│   ├── ...
├── Personal
│   ├── github.personal-ssh
│   ├── github.personal-ssh.pub
│   ├── ...
├── Labs
│   ├── gitlab.labs-ssh
│   ├── gitlab.labs-ssh.pub
│   ├── ...
├── Universidad
│   ├── gitlab.universidad-ssh
│   ├── gitlab.universidad-ssh.pub
│   ├── ...
├── ...
...
```

## 5. Enlaces

- https://linux.die.net/man/1/tree
- https://stackoverflow.com/questions/41052538/git-error-gpg-failed-to-sign-data/41054093#41054093
- https://stackoverflow.com/questions/41502146/git-gpg-onto-mac-osx-error-gpg-failed-to-sign-the-data
- https://newbedev.com/git-gpg-onto-mac-osx-error-gpg-failed-to-sign-the-data/
- https://stackoverflow.com/questions/72703308/git-commit-gpg-failed-to-sign-data-macos-monterey-all-things-up-to-date
- https://markentier.tech/posts/2021/02/github-with-multiple-profiles-gpg-ssh-keys/#gpg-key-management
- https://git-scm.com/docs/git-config
- https://www.ssh.com/academy/ssh/config
- https://docs.github.com/en/authentication
- https://dev.to/wes/how2-using-gpg-on-macos-without-gpgtools-428f
- https://docs.github.com/en/authentication/managing-commit-signature-verification/signing-commits
