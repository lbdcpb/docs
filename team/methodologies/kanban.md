# Kanban

Kanban ha ido ganando popularidad durante las últimas décadas. Nació para aplicarse a los procesos de fabricación y con el tiempo se convirtió en un territorio reclamado por los desarrolladores de software. Últimamente, ha empezado a ser reconocido por las entidades empresariales de diferentes ámbitos.

Cada vez más y más gente escucha sobre Kanban y a menudo aparecen malas interpretaciones. Entonces, ¿qué es el Kanban? Aquí tienes algunas de las cosas más importantes que debes saber sobre el método desde su origen y desarrollo hasta hoy en día.

## Historia

Kanban es un método para gestionar el trabajo que surgió en Toyota Production System (TPS). A finales de los años 40, Toyota implementó en su producción el sistema “just in time” (justo a tiempo”) que en realidad representa un sistema de arrastre. Esto significa que la producción se basa en la demanda de los clientes y no en la práctica tradicional “pull” de fabricar productos e intentar venderlos en el mercado.

Su exclusivo sistema de producción puso las bases del Lean Manufacturing (“producción ajustada”). Su propósito fundamental consiste en minimizar los desperdicios sin afectar la producción. El objetivo principal es crear más valor para el cliente sin generar más gastos.

**¿Qué significa Kanban?**

La palabra Kanban viene del japonés y traducida literalmente quiere decir tarjeta con signos o señal visual. El tablero más básico de Kanban está compuesto por tres columnas: “Por hacer”, “En proceso” y “Hecho”. Si se aplica bien y funciona correctamente, serviría como una fuente de información, ya que demuestra dónde están los cuellos de botella en el proceso y qué es lo que impide que el flujo de trabajo sea continuo e ininterrumpido.

## Método Kanban

A principios del siglo XXI, la industria del software se percató de que Kanban podía hacer un cambio real en la forma en la que se producían y entregaban los productos y los servicios. Se demostró que Kanban era conveniente no solo para la industria automotriz, sino también para cualquier otro tipo de industria. Así es como nació el método Kanban.

## Los 4 principios básicos de Método Kanban

David J. Anderson (reconocido como el líder de pensamiento de la adopción del Lean/Kanban para el trabajo de conocimiento) formuló el método Kanban como una aproximación al proceso evolutivo e incremental y al cambio de sistemas para las organizaciones de trabajo. El método está enfocado en llevar a cabo las tareas pendientes y los principios más importantes pueden ser divididos en cuatro principios básicos y seis prácticas.

### Principio 1: Empezar con lo que hace ahora

Kanban no requiere configuración y puede ser aplicado sobre flujos reales de trabajo o procesos activos para identificar los problemas. Por eso es fácil implementar Kanban en cualquier tipo de organización, ya que no es necesario realizar cambios drásticos.

### Principio 2: Comprometerse a buscar e implementar cambios incrementales y evolutivos

El método Kanban está diseñado para implementarse con una mínima resistencia, por lo que trata de pequeños y continuos cambios incrementales y evolutivos del proceso actual. En general, los cambios radicales no son considerados, ya que normalmente se encuentran con resistencias debidas al miedo o la incertidumbre del proceso.

### Principio 3: Respetar los procesos, las responsabilidades y los cargos actuales

Kanban reconoce que los procesos en curso, los roles, las responsabilidades y los cargos existentes pueden tener valor y vale la pena conservarlos. El método Kanban no prohíbe el cambio, pero tampoco lo prescribe. Alienta el cambio incremental, ya que no provoca tanto miedo como para frenar el progreso.

### Principio 4: Animar el liderazgo en todos los niveles

Este es el principio más novedoso de Kanban. Algunos de los mejores liderazgos surgen de actos del día a día de gente que está al frente de sus equipos. Es importante que todos fomenten una mentalidad de mejora continua (Kaizen) para alcanzar el rendimiento óptimo a nivel de equipo/ departamento/ empresa. Esto no puede ser una actividad a nivel de dirección.

## Las seis prácticas de Kanban
Aunque aceptar la filosofía de Kanban y embarcarse en el viaje de transición es el paso más importante, cada organización debe tener cuidado con los pasos prácticos. Hay seis prácticas centrales identificadas por David J. Anderson que deben estar presentes para una implementación con éxito.

### Visualizar el flujo de trabajo

Lo primero y lo más importante para usted es entender qué se necesita para el transcurso de un producto desde su pedido hasta su entrega. Solo después de entender cómo funciona actualmente el flujo de trabajo, puede aspirar a mejorarlo haciendo los ajustes necesarios.

Para visualizar su proceso en Kanban, necesitará un tablero con tarjetas y columnas. Cada columna del tablero representa un paso en su flujo de trabajo. Cada tarjeta Kanban representa un elemento de trabajo.

Cuando comience a trabajar en el elemento X, lo arrastra hasta la columna “Por hacer” y cuando el elemento esté acabado, lo mueve hasta la columna “Hecho”. De esta forma, puede fácilmente seguir el progreso y detectar los cuellos de botella.

### Eliminar las interrupciones

El cambio de enfoque puede dañar seriamente su proceso y la multitarea (o multitasking) podría provocar generación de desperdicios. Esta es la razón por la cual, la segunda práctica de Kanban se enfoca en establecer los límites del trabajo en proceso (los límites WIP). Si no hay límites de trabajo en proceso, no está haciendo Kanban.

Limitar el trabajo en proceso (WIP) significa que un sistema de arrastre (pull) se aplica sobre partes o sobre todo el flujo de trabajo. Establecer un número máximo de elementos por etapa asegura que una tarjeta se “arrastra” al siguiente paso sólo cuando hay capacidad disponible. Tales restricciones iluminarán rápidamente las áreas problemáticas en su flujo para que pueda identificarlas y resolverlas.

### Gestionar el flujo
La idea de implementar un sistema Kanban es crear un flujo continuo e ininterrumpido. Por flujo nos referimos al movimiento de elementos de trabajo a través del proceso de producción. Lo que interesa es la velocidad y la continuidad del movimiento.

Idealmente, queremos un flujo rápido e ininterrumpido. Esto significaría que nuestro sistema está creando valor rápidamente. O sea, minimizar el riesgo y evitar el coste de retraso, pero también hacerlo de manera previsible.

### Hacer las políticas explícitas (Fomentar la visibilidad)

No puede mejorar algo que no se entiende. Esta es la razón por la cual el proceso debe estar bien definido, publicado y promovido. Las personas no se asociarían ni participarían en algo que no creen que sea útil.

Cuando todos estén familiarizados con el objetivo común, podrán trabajar y tomar decisiones con respecto a cambios que les moverán hacia una dirección positiva.

### Circuitos de retroalimentación

Para que el cambio positivo ocurra, tenga éxito y sea duradero, se necesita hacer una cosa más. La filosofía Lean admite que las reuniones regulares son necesarias para la transferencia de conocimiento (circuitos de retroalimentación).

Tales son las reuniones diarias de pie para sincronizar el equipo. Se llevan a cabo frente al tablero Kanban y cada miembro comparte con los demás lo que él o ella hizo el día anterior y qué va a hacer el día de hoy.

También existen las reuniones para la revisión de entrega de servicios, la revisión de operaciones y la revisión de riesgos. Su frecuencia depende de muchos factores, pero la idea es que sean regulares, a una hora estrictamente fija, directas al grano y nunca innecesariamente largas.

La duración promedio ideal de una reunión de pie debe ser entre 10 y 15 minutos, y las demás reuniones pueden durar hasta una hora, en función del tamaño del equipo y los temas.

### Mejorar colaborando (usando modelos y el método científico)

La forma de lograr la mejora continua y el cambio sostenible dentro de una organización se consigue a través de la visión compartida para un futuro mejor y la comprensión colectiva de los problemas que deben superarse.

Los equipos que tienen un entendimiento compartido de las teorías sobre el trabajo, el flujo de trabajo, el proceso y el riesgo, tienen más probabilidades de crear una comprensión compartida de un problema y sugerir acciones de mejora que pueden acordarse por consenso.

## Ventajas de las Herramientas Kanban

Con el desarrollo de la tecnología, Kanban también va mejorando continuamente. Las soluciones del tablero digital Kanban se han desarrollado para superar los problemas que surgen en los equipos remotos.

La mayoría de las empresas grandes y en especial las startups tienen muchos empleados remotos. Los equipos a menudo son distribuidos por todo el mundo.

Ellos no pueden trabajar en un solo tablero físico y, por lo tanto, necesitan uno digital al que puedan acceder desde cualquier lugar. Los tableros Kanban en la nube son la forma más efectiva de conseguir que todos estén en la misma línea, ya que brindan acceso a toda la información desde cualquier dispositivo, en cualquier momento y muestran las acciones en vivo.

Además, el software Kanban permite un proceso analítico sofisticado para ayudarle a seguir el rendimiento en detalle, detectar los cuellos de botella e implementar los cambios necesarios.
Los tableros digitales Kanban también son fáciles de integrar con otros sistemas y pueden brindar una valiosa perspectiva de todo el proceso, ahorrar tiempo y aumentar la eficiencia.

## Kanban en pocas palabras

Kanban es algo más que notas adhesivas en la pared. La forma más fácil de entender Kanban es aceptar su filosofía y luego aplicarla a su trabajo diario. Si lee y entiende los cuatro principios básicos, la transición práctica parecerá lógica e incluso inevitable.

Visualizar el flujo de trabajo, establecer los límites del trabajo en proceso (WIP), gestionar el flujo, asegurar políticas explícitas y la mejora colaborativa llevarán a su proceso mucho más allá de lo que pueda imaginar. Recuerde organizar circuitos de retroalimentación regulares y el conjunto de todas estas piezas revelará el verdadero poder de Kanban.

Como ahora se está embarcando en un viaje hacia la comprensión de Kanban, esto es solo el inicio. Para obtener una comprensión más profunda de Kanban, explore los puntos fuertes de los tableros Kanban, los límites WIP y las tarjetas Kanban.

*fuente: https://kanbanize.com/es/recursos-de-kanban/primeros-pasos/que-es-kanban*
