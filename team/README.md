# Equipo de trabajo

## Guías orientativas

- [Gestión del proyecto](management/guidelines.md)  

## Metodologías y marcos de trabajo

- [Kanban](methodologies/kanban.md)
- [Scrum](methodologies/scrum.md)
- [XP](methodologies/xp.md)

## Bases

- [Manifiesto Agile](bases/manifesto.md)
- [Principios Agile](bases/principles.md)
