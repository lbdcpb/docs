# Guía orientativa para la gestión del proyecto

El objetivo de esta guía es brindar los conceptos necesarios para el desarrollo del producto digital, así como orientar en el uso de los mismos.

## Marco teórico

La guía está basada en los siguientes principios y metodologías:

* [Manifiesto ágil](../bases/manifesto.md)
* [Principios de la agilidad](../bases/principles.md)
* [Kanban](../methodologies/kanban.md)
* [Scrum](../methodologies/scrum.md)
* [XP](../methodologies/xp.md)

## Equipo

Además de las figuras del product owner y el scrum master, sugerimos que el staff de desarrolladores cuente también con los roles que XP define en la medida que sea posible.

## Ceremonias

Se recomiendan las establecidas en el marco de trabajo Scrum.

## Entregables

Además de los entregables asociados a la tarea, historia de usuario y/o incidencia se recomienda, siempre que sea posible, actualizar la documentación técnica y funcional del producto.  

## Disputas

La resolución de disputas, dependiendo del contexto, puede resolverla el equipo o el scrum master.

## Incidencias

Para la gestión de incidencias se sugiere el uso de tableros kanban.

*Fuentes: [Las metodologías ágiles más utilizadas y sus ventajas dentro de la empresa](https://www.iebschool.com/blog/que-son-metodologias-agiles-agile-scrum/) | [Gestión Ágil de proyectos: una guía completa](https://kanbanize.com/es/agiles/metodologia-agile)*
