# Docs

## Espacio de trabajo

### Configuraciones

- [Múltiples cuentas git](workspace/setup/configuration.md)

## Equipo de trabajo

### Guías orientativas

- [Gestión del proyecto](team/management/guidelines.md)  

### Metodologías y marcos de trabajo

- [Kanban](team/methodologies/kanban.md)
- [Scrum](team/methodologies/scrum.md)
- [XP](team/methodologies/xp.md)

### Bases

- [Manifiesto Agile](team/bases/manifesto.md)
- [Principios Agile](team/bases/principles.md)

## Herramientas
